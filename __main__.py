def encode_char(char, chave):
    n = ord(char)
    if 97 <= n <= 122:
        n = ord(char) + chave
        if n > 122:
            n = 97 + (n - 122) - 1
    return chr(n)


def encode_string(entrada, chave):
    result = ""
    for c in entrada:
        result += encode_char(c,chave)
    return result


def decode_char(char, chave):
    n = ord(char)
    if 97 <= n <= 122:
        n = ord(char) - chave
        if n < 97:
            n = 122 - (97 - n) + 1
    return chr(n)


def decode_string(entrada, chave):
    result = ""
    for c in entrada:
        result += decode_char(c, chave)
    return result


def codificar_com_lista(entrada, chaves: list) -> str:
    result=""
    i = 0
    for c in entrada:
        if 97 <= ord(c) <= 122:
            temp = encode_char(c, chaves[i])
            i += 1
            if i == len(chaves):
                i = 0
        else:
            temp = c
        result += temp
    return result


def descodificar_com_lista(entrada, chaves: list) -> str:
    result = ""
    i = 0
    for c in entrada:
        if 97 <= ord(c) <= 122:
            temp = decode_char(c, chaves[i])
            i += 1
            if i == len(chaves):
                i = 0
        else:
            temp = c
        result += temp
    return result


def main():
    texto = input("Introduza texto a codificar:")
    num = int(input("Introduza no chaves a utilizar:"))
    chaves = []
    while num > 0:
        chaves.append(int(input("Chave "+str(len(chaves))+":")))
        num -= 1
    encoded = codificar_com_lista(texto, chaves)
    print(encoded)
    decoded = descodificar_com_lista(encoded, chaves)
    print(decoded)

main()
